const { ethers } = require("hardhat")
require("dotenv").config({ path: ".env" });
require("@nomiclabs/hardhat-etherscan");
const { WHITELIST_CONTRACT_ADDRESS, METADATA_URL, CRYPTO_DEVS_NFT_CONTRACT_ADDRESS, CRYPTO_DEV_TOKEN_CONTRACT_ADDRESS,
    FEE, VRF_COORDINATOR, LINK_TOKEN, KEY_HASH } = require("../constants");

async function main() {
    // const whitelistContract = await ethers.getContractFactory("Whitelist")
    // const deployedWhitelistContract = await whitelistContract.deploy(10)
    // await deployedWhitelistContract.deployed()

    // const whitelistContract = WHITELIST_CONTRACT_ADDRESS;
    // const metadataURL = METADATA_URL;
    // const cryptoDevsContract = await ethers.getContractFactory("CryptoDevs");
    // const deployedCryptoDevsContract = await cryptoDevsContract.deploy(metadataURL, whitelistContract);

    // const cryptoDevsNFTContract = CRYPTO_DEVS_NFT_CONTRACT_ADDRESS;
    //     // const cryptoDevsTokenContract = await ethers.getContractFactory("CryptoDevToken");
    //     // const deployedCryptoDevsTokenContract = await cryptoDevsTokenContract.deploy(cryptoDevsNFTContract)
    //     // console.log(
    //     //     "Contract Address:",
    //     //     deployedCryptoDevsTokenContract.address
    //     // );

    // const fakeNFTMarketplace = await ethers.getContractFactory("FakeNFTMarketplace");
    // const deployedFakeNftMarketplace = await fakeNFTMarketplace.deploy();
    // await deployedFakeNftMarketplace.deployed();
    // console.log("FakeNFTMarketplace deployed to: ", deployedFakeNftMarketplace.address);
    //
    // const cryptoDevsDAO = await ethers.getContractFactory("CryptoDevsDAO");
    // const deployedCryptoDevsDAO = await cryptoDevsDAO.deploy(
    //     deployedFakeNftMarketplace.address,
    //     cryptoDevsNFTContract,
    //     { value: ethers.utils.parseEther("0.01") }
    // );
    // await deployedCryptoDevsDAO.deployed();
    // console.log("CryptoDevsDAO deployed to: ", deployedCryptoDevsDAO.address);

    // const cryptoDevTokenAddress = CRYPTO_DEV_TOKEN_CONTRACT_ADDRESS;
    // const exchangeContract = await ethers.getContractFactory("Exchange");
    // const deployedExchangeContract = await exchangeContract.deploy(cryptoDevTokenAddress);
    // await deployedExchangeContract.deployed();
    // console.log("Exchange Contract Address:", deployedExchangeContract.address);

    // URL from where we can extract the metadata for a LW3Punks
    // const metadataURL = "ipfs://Qmbygo38DWF1V8GttM1zy89KzyZTPU2FLUzQtiDvB7q6i5/";
    // const lw3PunksContract = await ethers.getContractFactory("LW3Punks");
    // const deployedLW3PunksContract = await lw3PunksContract.deploy(metadataURL);
    // await deployedLW3PunksContract.deployed();
    // console.log("LW3Punks Contract Address:", deployedLW3PunksContract.address);
    // sleep(15000);
    // Verify the contract after deploying
    // await hre.run("verify:verify", {
    //     address: deployedLW3PunksContract.address,
    //     constructorArguments: [metadataURL],
    // });

    // const randomWinnerGame = await ethers.getContractFactory("RandomWinnerGame");
    // const deployedRandomWinnerGameContract = await randomWinnerGame.deploy(VRF_COORDINATOR, LINK_TOKEN, KEY_HASH, FEE);
    // await deployedRandomWinnerGameContract.deployed();
    // console.log(
    //     "Verify Contract Address:",
    //     deployedRandomWinnerGameContract.address
    // );
    // console.log("Wait for etherscan to notice that the contract has been deployed");
    // await sleep(30000);
    // await hre.run("verify:verify", {
    //     address: "deployedRandomWinnerGameContract.address",
    //     constructorArguments: [VRF_COORDINATOR, LINK_TOKEN, KEY_HASH, FEE],
    // });
}

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
